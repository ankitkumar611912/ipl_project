const fs = require('fs');
const getJSONData = require('./csvToJsonConverter');
const { error } = require('console');


const matchesPath = '../data/matches.csv';
const deliveriesPath = '../data/deliveries.csv';
const outputPath = '../public/output/playerDismissedByOnePlayerMost.json'


function playerDismissOnePlayerMost(){

    let totalBatsMan = [];
    getJSONData(deliveriesPath).then((deliveryData) => {
        if(Array.isArray(deliveryData)){
            for(index = 0 ; index < deliveryData.length ; index++){
                const batsmanName = deliveryData[index].batsman;
                totalBatsMan.push(batsmanName);
            }
        }
        totalBatsMan = [...new Set(totalBatsMan)];

        let batsmanDismissed = {};
        for(let index = 0 ; index < totalBatsMan.length ; index++){
            let batsmanToFind = totalBatsMan[index];
            for(let deliveryIndex = 0 ; deliveryIndex < deliveryData.length ; deliveryIndex++){
                let currentBatsman = deliveryData[deliveryIndex].batsman;
                if(batsmanToFind === currentBatsman){
                    if(deliveryData[deliveryIndex].player_dismissed !== ''){
                        let dismissedByBowler = deliveryData[deliveryIndex].bowler;
                        if(batsmanDismissed[batsmanToFind]){
                            if(batsmanDismissed[batsmanToFind][dismissedByBowler]){
                                batsmanDismissed[batsmanToFind][dismissedByBowler] += 1;
                            }else{
                                batsmanDismissed[batsmanToFind][dismissedByBowler] = 1;
                                
                            }
                        }else{
                            batsmanDismissed[batsmanToFind] = {};
                            batsmanDismissed[batsmanToFind][dismissedByBowler] = 1;
                        }
                    }
                }
            }
        }
        let batsmanDismissedMostByOnePlayer = {};
        for(const player in batsmanDismissed){
            const maxDismissedByOnePlayer = Object.fromEntries(
                Object.entries(batsmanDismissed[player]).sort((a,b) => b[1]-a[1]).slice(0,1)
            )
            batsmanDismissedMostByOnePlayer[player] = maxDismissedByOnePlayer;
        }
        fs.writeFile(outputPath,JSON.stringify(batsmanDismissedMostByOnePlayer,null,2), (error) =>{
            if(error){
                console.log("Error in the data");
            }else{
                console.log("JSON file has been created at Output Path");
            }
        })
    })
}

playerDismissOnePlayerMost();