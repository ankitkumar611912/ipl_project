const fs = require('fs');
const getJSONData = require('./csvToJsonConverter');
const { error } = require('console');


const matchesPath = '../data/matches.csv';
const deliveriesPath = '../data/deliveries.csv';
const outputPath = '../public/output/wonTossWonMatch.json'

function wonTossWonMatch(){
    getJSONData(matchesPath).then((data) => {
        if(Array.isArray(data)){
            let wonTossWonMatchByTeam = {};
            for(let index = 0 ; index < data.length ; index++){
                const teamWinToss = data[index].toss_winner;
                let teamWinMatch = data[index].winner;
                if(teamWinMatch === "" || teamWinToss === ""){
                    continue;
                }
                if(teamWinToss === teamWinMatch){
                    if(teamWinMatch === "Rising Pune Supergiants"){
                        teamWinMatch = "Rising Pune Supergiant"
                    }
                    if(wonTossWonMatchByTeam[teamWinMatch]){
                        wonTossWonMatchByTeam[teamWinMatch] += 1;
                    }else{
                        wonTossWonMatchByTeam[teamWinMatch] = 1;
                    }
                }
            }
            fs.writeFile(outputPath,JSON.stringify(wonTossWonMatchByTeam,null,2),(error) =>{
                if(error){
                    console.log("Error in the data");
                }else{
                    console.log("JSON file has been created at Output Path");
                }
            })
        }else{
            return {};
        }   
    })
}

wonTossWonMatch();