const csv= require('csvtojson')
const path = require('path');


function csvToJSON(path){
 return  csv()
  .fromFile(path)
  .then((jsonObj)=>{
    return jsonObj
  })
}

module.exports = csvToJSON;