const fs = require('fs');
const getJSONData = require('./csvToJsonConverter');
const { error } = require('console');


const matchesPath = '../data/matches.csv';
const deliveriesPath = '../data/deliveries.csv';
const outputPath = '../public/output/mostEconomicalBowler.json'


function mostEconimicalBowler(){
    getJSONData(matchesPath).then((data)=>{
        if(Array.isArray(data)){
            const matchId2015 = [];
            for(let index = 0 ; index < data.length ; index++){
                if(data[index].season === '2015'){
                    matchId2015.push(data[index].id);
                }
            }
            
            const bowlerStat = {};
            getJSONData(deliveriesPath).then((data) => {
                for(let index = 0 ; index < data.length ; index++){
                    if(matchId2015.includes(data[index].match_id)){
                        
                        const bowler = data[index].bowler;
                        const totalRuns = data[index].total_runs - data[index].penalty_runs;
                        
                        const ballNotToAdd = data[index].wide_runs > 0 || data[index].nobowl_runs > 0;
                        
                        let runs=0;
                        let balls=0;
                        if(bowlerStat[bowler]){
                            bowlerStat[bowler].balls++;
                            if(!ballNotToAdd){
                                bowlerStat[bowler].runs+=totalRuns;
                            }
                        }else {
                            bowlerStat[bowler] = {
                            runs: ballNotToAdd ? 0 : totalRuns,
                            balls: ballNotToAdd ? 0 : 1,
                            };
                        }      
                    }
                }
                const economyRate={};
                for (const bowler in bowlerStat) {
                    const { runs, balls } = bowlerStat[bowler];
                    economyRate[bowler] = (runs / balls) * 6; 
                }
                const sortedEconomyRate = Object.fromEntries(
                    Object.entries(economyRate)
                    .sort((a, b) => a[1] - b[1]).slice(0,10)
                );
                const sortedEconomyRateJSON = JSON.stringify(sortedEconomyRate, null, 2);
                fs.writeFile(outputPath,sortedEconomyRateJSON,(error) =>{
                    if(error){
                        console.log("Error in the data");
                    }else{
                        console.log("JSON file has been created at Output Path");
                    }
                })
            });
        }
        else{
            return {};
        }
    });
    

}

mostEconimicalBowler();
  