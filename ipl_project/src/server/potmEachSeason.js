const fs = require('fs');
const getJSONData = require('./csvToJsonConverter');
const { error } = require('console');


const matchesPath = '../data/matches.csv';
const deliveriesPath = '../data/deliveries.csv';
const outputPath = '../public/output/potmEachSeason.json'


function potmEachSeason(){
    getJSONData(matchesPath).then((data) => {
        if(Array.isArray(data)){
            const potmEverySeason = {};
            for(let index = 0 ; index < data.length ; index++){
                let currentSeason = data[index].season;
                let player = data[index].player_of_match;
                if(player === '')
                {
                    continue;
                }
                if(potmEverySeason[currentSeason]){
                    if(potmEverySeason[currentSeason][player]){
                        potmEverySeason[currentSeason][player] += 1;
                    }else{
                        potmEverySeason[currentSeason][player] = 1;
                    }
                }
                else{
                    potmEverySeason[currentSeason] = {};
                    potmEverySeason[currentSeason][player] = 1;
                }
            }
            const highestPotmEachSeason = {};

            for(const season in potmEverySeason){
                const playerWithMostMOM = Object.fromEntries(
                    Object.entries(potmEverySeason[season]).sort((a,b) => b[1] - a[1]).slice(0,1)
                );
                highestPotmEachSeason[season] = playerWithMostMOM;
            }
            
            // console.log(highestPotmEachSeason);

            fs.writeFile(outputPath,JSON.stringify(highestPotmEachSeason,null,2),(error) => {
                if(error){
                    console.log("Error in the data");
                }else{
                    console.log("JSON file has been created at Output Path");
                }
            })

        }
        else{
            return {};
        }
    });
}

potmEachSeason();

