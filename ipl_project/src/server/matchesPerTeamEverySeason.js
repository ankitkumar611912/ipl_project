const fs = require('fs');
const getJSONData = require('./csvToJsonConverter');
const { error } = require('console');

const matchesPath = '../data/matches.csv';
const deliveriesPath = '../data/deliveries.csv';
const outputPath = '../public/output/matchesPerTeamEveryYear.json';

 function matchesPerTeamPerYear(){
    getJSONData(matchesPath).then((data) => {
        let matchesPerTeamEveryYear = {};
        if(Array.isArray(data)){
            for(let index = 0 ; index < data.length ; index++){
                let currentSeason = data[index].season;
                let winner = data[index].winner;

                if(winner === ""){
                    continue;
                }
                
                if(matchesPerTeamEveryYear.hasOwnProperty(currentSeason)){
                    if(matchesPerTeamEveryYear[currentSeason].hasOwnProperty(winner)){
                        matchesPerTeamEveryYear[currentSeason][winner] += 1;
                    }
                    else{
                        matchesPerTeamEveryYear[currentSeason][winner] = 1;
                    }
                }
                else{
                    matchesPerTeamEveryYear[currentSeason] ={};
                    matchesPerTeamEveryYear[currentSeason][winner] = 1;
                }
                
            }
            fs.writeFile(outputPath,JSON.stringify(matchesPerTeamEveryYear,null,2),(error)=>{
                if(error){
                    console.log("Error in the data");
                }else{
                    console.log("JSON file has been created at Output Path");
                }
            })
        }
        else{
            return {};
        }
    });
}

matchesPerTeamPerYear();
