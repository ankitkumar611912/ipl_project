const fs = require('fs');
const getJSONData = require('./csvToJsonConverter');
const { error } = require('console');

const matchesPath = '../data/matches.csv';
const deliveriesPath = '../data/deliveries.csv';
const outputPath = '../public/output/matchesPerYear.json';

 function matchesPerYear(){
    getJSONData(matchesPath).then((data) => {
        let matchesPerYear = {};
        if(Array.isArray(data)){
            for(let index = 0 ; index < data.length ; index++){
                let currentSeason = data[index].season;
                if(matchesPerYear.hasOwnProperty(currentSeason)){
                    matchesPerYear[currentSeason] += 1;
                }
                else{
                    matchesPerYear[currentSeason] = 1;
                }
            }
            fs.writeFile(outputPath,JSON.stringify(matchesPerYear,null,2),(error)=>{
                if(error){
                    console.log("Error in the data");
                }else{
                    console.log("JSON file has been created at Output Path");
                }
            })
        }
        else{
            return {};
        }
    });
}

matchesPerYear();
