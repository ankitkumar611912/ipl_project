const fs = require('fs');
const getJSONData = require('./csvToJsonConverter');
const { error } = require('console');


const matchesPath = '../data/matches.csv';
const deliveriesPath = '../data/deliveries.csv';
const outputPath = '../public/output/extraRunConcededPerTeam.json'


function extraRunConceded(){
    getJSONData(matchesPath).then((data) => {
        if(Array.isArray(data)){
            let matchId = {};
            for(let index = 0 ; index < data.length ; index++){
                let currentSeason = data[index].season;
                if(currentSeason === '2016'){
                    let id = data[index].id;
                    matchId[id] = currentSeason;
                }
            }
            getJSONData(deliveriesPath).then((data) =>{
                let extraPerTeam = {};
                for(let index = 0 ; index < data.length  ; index++){
                    const currentMatchId = data[index].match_id;
                    const season = matchId[currentMatchId];
                    if(season === '2016'){
                        let team = data[index].bowling_team;
                        let extraRuns = Number(data[index].extra_runs)
                        if(extraPerTeam[team]){
                            extraPerTeam[team] += extraRuns;
                        }else{
                            extraPerTeam[team] = extraRuns;
                        }
                    }
                }
                fs.writeFile(outputPath,JSON.stringify(extraPerTeam,null,2),(error)=>{
                    if(error){
                        console.log("Error in the data");
                    }else{
                        console.log("JSON file has been created at Output Path");
                    }
                })
            })
    
            
        }
        else{
            return {};
        }
    });
}

extraRunConceded();
