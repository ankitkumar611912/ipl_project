const fs = require('fs');
const getJSONData = require('./csvToJsonConverter');
const { error } = require('console');

const matchesPath = '../data/matches.csv';
const deliveriesPath = '../data/deliveries.csv';
const outputPath = '../public/output/mostEconomicalInSuperOver.json';


function mostEconomical(){

    getJSONData(deliveriesPath).then((deliveryData) =>{
        
        let superOverRun = {};
        let superOverBall = {};
        for(let index = 0 ; index < deliveryData.length ; index++){
            const currentOver = Number.parseInt(deliveryData[index].is_super_over);
            const currentBowler = deliveryData[index].bowler;
            
            if(currentOver){
                const wide = Number.parseInt(deliveryData[index].wide_runs);
                const noball = Number.parseInt(deliveryData[index].noball_runs);
                const totalRun = Number.parseInt(deliveryData[index].total_runs) - (Number.parseInt(deliveryData[index].bye_runs) + Number.parseInt(deliveryData[index].legbye_runs));
                
                let ball = 1;
                if(wide > 0 || noball > 0){
                    ball = 0;
                }

                if(superOverRun[currentBowler]){
                    superOverRun[currentBowler] += totalRun;
                }else{
                    superOverRun[currentBowler] = totalRun;
                }
                if(superOverBall[currentBowler]){
                    superOverBall[currentBowler] += ball;
                }
                else{
                    superOverBall[currentBowler] = ball;
                }
            }
            
        }
        // console.log(superOverRun);
        // console.log(superOverBall);
        const economyRate = {};

        for(let key in superOverBall){
            economyRate[key] = (superOverRun[key]/superOverBall[key])*6
        }
        // console.log(economyRate);

        const mostEconimicalBowler = Object.fromEntries(
            Object.entries(economyRate).sort((a,b) => a[1] -b[1]).slice(0,1)
        )
        fs.writeFile(outputPath,JSON.stringify(mostEconimicalBowler,null,2) , (error) => {
            if(error){
                console.log("Error in the data");
            }else{
                console.log("JSON file has been created at Output Path");
            }
        })

    })

}


mostEconomical();
