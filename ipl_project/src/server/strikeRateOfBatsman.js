const fs = require('fs');
const getJSONData = require('./csvToJsonConverter');
const { error } = require('console');


const matchesPath = '../data/matches.csv';
const deliveriesPath = '../data/deliveries.csv';
const outputPath = '../public/output/strikeRateOfaBatsman.json';


function strikeRateOfaBatsman(){
    let totalBatsMan = [];
    getJSONData(deliveriesPath).then((deliveryData) => {
        if(Array.isArray(deliveryData)){
            for(index = 0 ; index < deliveryData.length ; index++){
                const batsmanName = deliveryData[index].batsman;
                totalBatsMan.push(batsmanName);
            }
        }
        totalBatsMan = [...new Set(totalBatsMan)];
        // console.log(totalBatsMan);
        const matchId = {};
        getJSONData(matchesPath).then((matchData) => {
            if(Array.isArray(matchData)){
                for(let index = 0 ; index < matchData.length ; index++){
                    const currentSeason = matchData[index].season;
                    const currentMatchId = matchData[index].id;
                    matchId[currentMatchId] = currentSeason;
                }
            }
            const batsmanStrikeratePerSeason = {};
            for(let index1 = 0 ; index1 < totalBatsMan.length ; index1++){
                const batsmanToFind = totalBatsMan[index1];
                let ballPerSeason = {};
                let runPerSeason = {}
                for(let index = 0 ; index < deliveryData.length ; index++){
                    const currentId = deliveryData[index].match_id;
                    const currentBatsmanName = deliveryData[index].batsman;
                    if(batsmanToFind === currentBatsmanName){
                        const currentSeason = matchId[deliveryData[index].match_id];
                        const run = Number.parseInt(deliveryData[index].batsman_runs);
                        let balls = Number.parseInt(deliveryData[index].ball);
                        if(deliveryData[index].wide_runs > 0){
                            balls -= 1;
                        }
                        if(ballPerSeason[currentSeason]){
                            ballPerSeason[currentSeason] += 1;
                        }else{
                            ballPerSeason[currentSeason]  = 1;
                        }
                        if(runPerSeason[currentSeason]){
                            runPerSeason[currentSeason] += run;
                        }else{
                            runPerSeason[currentSeason] = run;
                        }
                    }
                }
                let strikeRatePerSeason = {};
                for(const key in runPerSeason){
                    strikeRatePerSeason[key] = ((runPerSeason[key]/ballPerSeason[key])*100).toFixed(3);
                }
                batsmanStrikeratePerSeason[batsmanToFind] = strikeRatePerSeason;
            }
            fs.writeFile(outputPath,JSON.stringify(batsmanStrikeratePerSeason,null,2),(error) =>{
                if(error){
                    console.log("Error in the data");
                }else{
                    console.log("JSON file has been created at Output Path");
                }
            })
            
        })
        
    
    })
    
    
}

strikeRateOfaBatsman();